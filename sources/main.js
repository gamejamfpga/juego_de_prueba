var state, states
var textures = {};
var TIMESTEP = 1 / 5;
var canvas;
var song;
var speed = 0;
var pastPos = 0;
function preload() {
	textures.bus = loadImage("rsc/bus.png");
	textures.bg = loadImage("rsc/bg.png");
	textures.arrow = loadImage("rsc/arrow.png");
	textures.fondo = loadImage("rsc/backg.jpg")
	song = loadSound('rsc/temaso.wav');
}

function setup() {
	//song.loop();
	canvas = createCanvas(800, 600);
  canvas.drawingContext.imageSmoothingEnabled = false;
	state = 0;
	states = [
		new Level()
	]
	opciones =
	[
		new menuBox(50,50,200,100),
		new menuBox(50,200,200,100),
		new menuBox(50,350,200,100)
	]
	menuInicial = new menuOption(50,50,100,100,3,10);
}

function draw() {

 	background(textures.fondo);

	stroke(0);
	strokeWeight(5)
	fill(255);
	// text(menuInicial.getIndexSelected(), 400, 60);
	// text(menuInicial.getIndexClicked(), 400, 120);
	// text(menuInicial.getIndexInside(), 400, 180);
	menuInicial.setColorPasive([30,30,30,64],255,5);
	menuInicial.setColorInside([64,64,64,64],255,5);
	menuInicial.setColorCliked([128,128,128,64],255,5);
	menuInicial.insertText(["Iniciar juego","Opciones","Salir"],20,[0,1,2]);
	menuInicial.drawMenu();
	//menuInicial.boxes[0].texto.textSize(20).textAlign(CENTER,CENTER);
//	background(255);
//	states[state].update(TIMESTEP);
//	states[state].draw();
//	speed = speed + 0.001;
//	speed = (this.states[state].player.chassis.body.position[0]-pastPos)/100+1;
//	pastPos = this.states[state].player.chassis.body.position[0];
//	//speed = int(this.states[state].player.carvel)/1000;
//  	song.rate(speed);
	//this.song.rate(this.Level.carVel);
}

function polygon(x, y, radius, npoints) {
  var angle = TWO_PI / npoints;
  beginShape();
  for (var a = 0; a < TWO_PI; a += angle) {
    var sx = x + cos(a) * radius;
    var sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
